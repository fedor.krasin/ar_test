using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pokemon : MonoBehaviour
{
    private void CatchPokemon()
    {
        Debug.Log("caught!");
    }
    
    private void OnMouseDown()
    {
        CatchPokemon();
        Destroy(gameObject);
    }
}
